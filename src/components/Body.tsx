import React from "react";

//typescript generics
interface IProps {}
interface IState {
  count: number;
}

export default class Body extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      count: 0,
    };
  }

  increment = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  decrement = () => {
    this.setState({
      count: this.state.count - 1,
    });
  };

  render() {
    return (
      <div>
        <p>Body Works!</p>
        <p>Counter: {this.state.count}</p>
        <button onClick={this.increment}>increment</button>
        <button onClick={this.decrement}>decrement</button>
      </div>
    );
  }
}
