import React from "react";
import shortid from "shortid";

interface IProps {
  onSubmit: any;
}
interface IState {
  title?: string;
  titleError?: string;
  content?: string;
  contentError?: string;
}
const initialState = {
  title: "",
  titleError: "",
  content: "",
};

export default class ToDoForm extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = initialState;
  }

  validation = () => {
    this.setState({ titleError: "", contentError: "" });
    let titleError = "Title cannot be empty!";

    if (!this.state.title) {
      this.setState({ titleError });
      return false;
    }

    return true;
  };

  handleInputChange = (event: any) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event: any) => {
    event.preventDefault();
    const isValid = this.validation();
    if (isValid) {
      this.props.onSubmit({
        id: shortid.generate(),
        title: this.state.title,
        content: this.state.content,
        completed: false,
      });

      this.setState(initialState);
    } else {
      console.log("Form not valid");
    }
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Enter a title for the <b>ToDo</b> item:
        </label>
        <br></br>
        <input
          name="title"
          value={this.state.title}
          onChange={this.handleInputChange}
        ></input>
        {this.state.titleError ? <div>{this.state.titleError}</div> : null}
        <br></br>
        <label>
          Description of the <b>ToDo</b> item:
        </label>
        <br></br>
        <textarea
          name="content"
          value={this.state.content}
          onChange={this.handleInputChange}
        ></textarea>
        <br></br>
        <button type="submit">submit</button>
      </form>
    );
  }
}
