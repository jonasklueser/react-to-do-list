import React from "react";
import { TODO } from "../TODO";
interface IProps {
  todo: TODO;
  refreshCounter: any;
  deleteToDo: any;
}
interface IState {}
export default class ToDo extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {};
  }

  handleCompleted = (event: any) => {
    this.props.todo.completed = !this.props.todo.completed;
    this.props.refreshCounter(this.props.todo.completed);
  };

  handleDeleteToDo = (event: any) => {
    this.props.deleteToDo(this.props.todo.id);
  };

  render() {
    return (
      <div
        key={this.props.todo.id}
        className={`todoItem ${this.props.todo.completed ? "completed" : null}`}
      >
        <h3>{this.props.todo.title}</h3>
        {this.props.todo.content ? <p>{this.props.todo.content}</p> : null}
        <label>Completed?</label>
        <input
          type="checkbox"
          checked={this.props.todo.completed}
          onChange={this.handleCompleted}
        ></input>
        <button className="deleteBtn" onClick={this.handleDeleteToDo}>
          Delete ToDo
        </button>
      </div>
    );
  }
}
