import React from "react";
import ToDoForm from "./ToDoForm";
import ToDo from "./ToDo";
import { TODO } from "../TODO";

interface IProps {}
interface IState {
  todos: TODO[];
  activeCount: number;
  filter: string;
  toogleCompleted: boolean;
}

export default class ToDoList extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      todos: [],
      activeCount: 0,
      filter: "all",
      toogleCompleted: true,
    };
  }

  addToDo = (todo: TODO) => {
    this.setState({
      //create a copy of the existing todo array and add the new todo to the beginning
      todos: [todo, ...this.state.todos],
      activeCount: this.state.activeCount + 1,
    });
  };

  refreshCounter = (completed: boolean) => {
    completed
      ? this.setState({
          activeCount: this.state.activeCount - 1,
        })
      : this.setState({
          activeCount: this.state.activeCount + 1,
        });
  };

  changeFilter = (event: any) => {
    this.setState(
      {
        filter: event.target.value,
      },
      () => {}
    );
  };

  handleDeleteToDo = (id: string) => {
    this.setState({
      todos: this.state.todos.filter((todo) => todo.id !== id),
      activeCount: this.state.activeCount - 1,
    });
  };

  handleDeleteAllCompleted = () => {
    this.setState({
      todos: this.state.todos.filter((todo) => !todo.completed),
    });
  };

  handleToggleAllCompleted = () => {
    var todos = this.state.todos;
    todos.forEach((todo) => {
      todo.completed = this.state.toogleCompleted;
    });

    this.setState({
      todos,
      toogleCompleted: !this.state.toogleCompleted,
      activeCount: this.state.toogleCompleted ? 0 : this.state.todos.length,
    });
  };

  render() {
    return (
      <div>
        <div>
          <h2>ToDo:</h2>
          <h4>Active ToDo's: {this.state.activeCount}</h4>
          <ToDoForm onSubmit={this.addToDo} />
          <br></br>
          <label>Filter ToDo's: </label>
          <select value={this.state.filter} onChange={this.changeFilter}>
            <option>all</option>
            <option>active</option>
            <option>completed</option>
          </select>
          <br></br>
          <br></br>
          <button onClick={this.handleToggleAllCompleted}>
            Toggle all items as{" "}
            {this.state.toogleCompleted ? "completed" : "active"}
          </button>
          {this.state.activeCount < this.state.todos.length ? (
            <div>
              <button onClick={this.handleDeleteAllCompleted}>
                Delete all completed ToDo's
              </button>
              <br></br>
            </div>
          ) : null}
        </div>

        {this.state.todos.length ? (
          this.state.todos.map((todo) =>
            this.state.filter == "active" && !todo.completed ? (
              <ToDo
                todo={todo}
                key={todo.id}
                refreshCounter={this.refreshCounter}
                deleteToDo={this.handleDeleteToDo}
              />
            ) : this.state.filter == "completed" && todo.completed ? (
              <ToDo
                todo={todo}
                key={todo.id}
                refreshCounter={this.refreshCounter}
                deleteToDo={this.handleDeleteToDo}
              />
            ) : this.state.filter == "all" ? (
              <ToDo
                todo={todo}
                key={todo.id}
                refreshCounter={this.refreshCounter}
                deleteToDo={this.handleDeleteToDo}
              />
            ) : null
          )
        ) : (
          <p>No active ToDo's.</p>
        )}
      </div>
    );
  }
}
