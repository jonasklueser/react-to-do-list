import React from "react";

type props = {
  info: {
    count: number;
    name: string;
  };
};

const Header = (props: props) => {
  return (
    <div id="header">
      <h1>To Do:</h1>
      <p>
        Hallo {props.info.name} du hast {props.info.count} ToDo's zu erledigen!
      </p>
    </div>
  );
};

export default Header;
