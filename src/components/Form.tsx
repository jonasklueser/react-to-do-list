import React from "react";

interface IProps {}
interface IState {
  name?: string;
  email?: string;
  password?: string;
  nameError?: string;
  emailError?: string;
  passwordError?: string;
}

const initialState = {
  name: "",
  email: "",
  password: "",
  nameError: "",
  emailError: "",
  passwordError: "",
};

export default class Form extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = initialState;
  }

  validate = () => {
    let nameError = "name field cant be empty";
    let emailError = "enter a valid email adress";
    let passwordError = "";

    if (!this.state.name) {
      this.setState({ nameError });
      return false;
    }
    if (!this.state.email?.includes("@")) {
      this.setState({ emailError });
      return false;
    }

    return true;
  };

  handleChange = (event: any) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event: any) => {
    const isValid = this.validate();
    event.preventDefault();
    if (isValid === true) {
      console.log(this.state);
      this.setState(initialState);
    }
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          name="name"
          placeholder="name"
          value={this.state.name}
          onChange={this.handleChange}
        ></input>
        {this.state.nameError ? <div>{this.state.nameError}</div> : null}
        <br></br>
        <input
          name="email"
          placeholder="email"
          value={this.state.email}
          onChange={this.handleChange}
        ></input>
        {this.state.emailError ? <div>{this.state.emailError}</div> : null}
        <br></br>
        <input
          type="password"
          name="password"
          placeholder="password"
          value={this.state.password}
          onChange={this.handleChange}
        ></input>
        {this.state.passwordError ? (
          <div>{this.state.passwordError}</div>
        ) : null}
        <button type="submit">submit</button>
      </form>
    );
  }
}
