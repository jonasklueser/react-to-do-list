export interface TODO {
  id: string;
  title: string;
  content: string;
  completed: boolean;
}
