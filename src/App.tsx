import React from "react";
import "./App.css";
import Header from "./components/Header";
import Body from "./components/Body";
import Form from "./components/Form";
import ToDoList from "./components/ToDoList";

interface IProps {}
interface IState {
  visible_body: boolean;
}
export default class App extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      visible_body: true,
    };
  }

  toggleBody = () => {
    this.setState({ visible_body: !this.state.visible_body });
  };

  render() {
    return <ToDoList />;
  }
  /*render() {
    const buttonText = this.state.visible_body ? "hide" : "show";
    return (
      <div>
        <Header
          info={{
            count: 5,
            name: "huseyner",
          }}
        />
        <div className={this.state.visible_body ? "visible" : "hidden"}>
          <Body />
        </div>
        <button onClick={this.toggleBody}>{buttonText}</button>
        <br></br>
        <hr></hr>
        <Form />
      </div>
    );
  }*/
}
